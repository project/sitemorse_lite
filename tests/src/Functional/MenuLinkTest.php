<?php

namespace Drupal\Tests\sitemorse_lite\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test for the menu link in the toolbar.
 *
 * @package Drupal\Tests\sitemorse_lite\Functional
 * @group sitemorse_lite
 */
class MenuLinkTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['toolbar', 'sitemorse_lite'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = "stark";

  /**
   * Create a user with access to sitemorse.
   *
   * @param array $permissions
   *   Extra permissions.
   *
   * @return \Drupal\user\Entity\User|false
   *   A fully loaded user object with pass_raw property, or FALSE if account
   *   creation fails.
   */
  protected function createSitemorseUser(array $permissions = []) {
    $permissions = array_merge(
      ['run sitemorse check', 'access toolbar'],
      $permissions
    );
    return $this->drupalCreateUser($permissions);
  }

  /**
   * Test menu link shows if user has access to run checks.
   */
  public function testSitemorseMenuLinkShows() {
    $account = $this->createSitemorseUser();
    $this->drupalLogin($account);

    $this->drupalGet('<front>');
    $this->assertSession()->elementExists('css', 'a.sitemorse-check-url');
  }

  /**
   * Test menu link does not shows if user does not have access to run checks.
   */
  public function testSitemorseMenuLinkDoesNotShowIfNoPermission() {
    $account = $this->drupalCreateUser(['access toolbar']);
    $this->drupalLogin($account);

    $this->drupalGet('<front>');
    $this->assertSession()->elementNotExists('css', 'a.sitemorse-check-url');
  }

  /**
   * Test menu link is inactive if user is on an admin page.
   */
  public function testSitemorseMenuLinkIsInactiveOnAdminPages() {
    $account = $this->createSitemorseUser(['access administration pages']);
    $this->drupalLogin($account);

    $this->drupalGet('/admin');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'div.sitemorse-check-url.sitemorse-inactive');
  }

}
