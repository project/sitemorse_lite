<?php

namespace Drupal\sitemorse_lite\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sitemorse settings administration form.
 */
class SitemorseSettingsForm extends ConfigFormBase {

  /**
   * Client for checking the licence.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * SitemorseSettingsForm constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $client) {
    parent::__construct($config_factory);
    $this->httpClient = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sitemorse_lite.settings');

    $form['main_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Main settings'),
      '#open' => TRUE,
    ];
    $form['main_settings']['license_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('License key'),
      '#required' => TRUE,
      '#default_value' => $config->get('license_key'),
      '#description' => $this->t('Some licenses require verification. You will not be able to use Sitemorse until the key is verified at least once.'),
    ];
    $form['main_settings']['verify'] = [
      '#type' => 'button',
      '#value' => $this->t('Verify license key'),
      '#ajax' => [
        'callback' => '::verifyLicence',
        'event' => 'click',
        'wrapper' => 'verify-output',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
      '#suffix' => '<div id="verify-output"></div>',
    ];
    $form['main_settings']['licence_verification_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sitemorse licence verification URL'),
      '#default_value' => $config->get('licence_verification_url'),
      '#description' => $this->t('Verify licence against this Sitemorse environment.'),
    ];

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#open' => FALSE,
    ];
    $form['advanced_settings']['ssl_connection'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('SSL connection'),
      '#description' => $this->t('SSL connection to Sitemorse server.'),
      '#default_value' => $config->get('ssl_connection'),
    ];
    $form['advanced_settings']['post_allowed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('POST allowed'),
      '#description' => $this->t('Allow Sitemorse server to process POST requests.'),
      '#default_value' => $config->get('post_allowed'),
    ];
    $form['advanced_settings']['proxy_hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy hostname'),
      '#description' => $this->t('Local proxy (if used).'),
      '#default_value' => $config->get('proxy_hostname_port'),
    ];
    $form['advanced_settings']['proxy_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy port'),
      '#description' => $this->t('Local proxy port (if used).'),
      '#default_value' => $config->get('proxy_port'),
    ];
    $form['advanced_settings']['server_hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sitemorse server hostname'),
      '#description' => $this->t('Sitemorse server hostname. Normally sci.sitemorse.com'),
      '#default_value' => $config->get('server_hostname'),
    ];
    $form['advanced_settings']['server_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sitemorse server port'),
      '#description' => $this->t('Normally 5371 or 5372 when SSL connection is enabled'),
      '#default_value' => $config->get('server_port'),
    ];
    $form['advanced_settings']['additional_http_headers'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additional HTTP headers'),
      '#default_value' => $config->get('additional_http_headers'),
    ];
    $form['advanced_settings']['additional_query_string'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional query string'),
      '#default_value' => $config->get('additional_query_string'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Return a list of the config keys contained in sitemorse.settings.
   */
  public function defaultKeys() {
    return [
      'license_key',
      'ssl_connection',
      'post_allowed',
      'proxy_hostname',
      'proxy_port',
      'server_hostname',
      'server_port',
      'additional_http_headers',
      'additional_query_string',
      'licence_verification_url',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sitemorse_lite_system_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('sitemorse_lite.settings');

    foreach ($this->defaultKeys() as $key) {
      $config->set($key, $form_state->getValue($key));
    }

    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sitemorse_lite.settings'];
  }

  /**
   * Ajax callback to verify the licence key.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Render array.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   On error.
   */
  public function verifyLicence(array &$form, FormStateInterface $form_state) {
    $licence = $form_state->getValue('license_key');
    $url = $form_state->getValue('licence_verification_url');

    try {
      $this->httpClient->request('post', $url, [
        'form_params' => [
          'licence' => $licence,
        ],
      ]);

      $message = "Verified.";
    }
    catch (ClientException $e) {
      $message = $e->getMessage();
      $this->logger('sitemorse_lite')->error($e->getMessage());

      if ($e->getCode() === 404) {
        $message = "Key not found.";
      }
    }

    return ['#markup' => "<div id=\"verify-output\"><p>$message</p></div>"];
  }

}
