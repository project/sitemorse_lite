<?php

namespace Drupal\sitemorse_lite\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\sitemorse_lite\Sitemorse\Checker;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Controller for running the sitemorse check.
 */
class CheckController implements ContainerInjectionInterface {

  /**
   * The sitemorse checker service.
   *
   * @var \Drupal\sitemorse_lite\Sitemorse\Checker
   */
  private $checker;

  /**
   * CheckController constructor.
   */
  public function __construct(Checker $checker) {
    $this->checker = $checker;
  }

  /**
   * Run sitemorse check for a given URL.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response for client.
   */
  public function checkUrl(Request $request) {
    $url = $request->query->get('url');

    if (empty($url)) {
      throw new BadRequestHttpException();
    }

    try {
      $result = $this->checker->checkUrl($url);
      return new JsonResponse(['url' => $result->getResultUrl()]);
    }
    catch (\Exception $e) {
      // Unwrap the original message from the exception.
      $reflected = new \ReflectionClass(\SCIClient::class);
      $fn = $reflected->getFileName();
      $error = substr($e->getMessage(), 0, strpos($e->getMessage(), "\n"));
      if (preg_match("|^(.+) in $fn|", $e->getMessage(), $m)) {
        $error = $m[1];
      }
      $response = new JsonResponse(['error' => $error]);
      $response->setStatusCode(500);
      return $response;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sitemorse_lite.checker')
    );
  }

}
