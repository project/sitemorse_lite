<?php

namespace Drupal\sitemorse_lite;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;

/**
 * Control the sitemorse menu link.
 *
 * @package Drupal\sitemorse_lite
 */
class MenuLinkBuilder {
  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * The current route.
   *
   * @var \Symfony\Component\Routing\Route
   */
  private $route;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $account;

  /**
   * MenuLinkBuilder constructor.
   */
  public function __construct(RouteMatchInterface $routeMatch, AccountInterface $account, TranslationInterface $stringTranslation) {
    $this->route = $routeMatch->getRouteObject();
    $this->account = $account;
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * Is the menu link enabled for the current URL?
   *
   * @return bool
   *   TRUE if enabled.
   */
  public function isEnabled() {
    return !$this->route->getOption('_admin_route');
  }

  /**
   * Cacheable metadata for the link.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   Cacheable metadata for the link.
   */
  public function cacheableMetadata() {
    $cacheableMetadata = new CacheableMetadata();
    $cacheableMetadata->setCacheContexts(['user.permissions', 'url.path']);
    return $cacheableMetadata;
  }

  /**
   * Build the sitemorse menu link render array.
   *
   * @return array
   *   Render array.
   */
  public function buildMenuLink() {
    $build = [];
    $this->cacheableMetadata()->applyTo($build);

    if (!$this->account->hasPermission('run sitemorse check')) {
      return $build;
    }

    $attributes = [
      'class' => ['toolbar-icon', 'toolbar-item', 'sitemorse-check-url'],
    ];

    $title = $this->t('Sitemorse check');
    $link = [
      '#type' => 'link',
      '#title' => $title,
      '#url' => Url::fromRoute('sitemorse_lite.check-url'),
      '#options' => [
        'attributes' => $attributes + ['title' => $this->t('Sitemorse check')],
      ],
    ];

    if (!$this->isEnabled()) {
      $attributes['class'][] = 'sitemorse-inactive';
      $attributes['title'][] = $this->t('Sitemorse is disabled on administrative URLs.');
      $link = [
        "#type" => "html_tag",
        "#tag" => "div",
        "#attributes" => $attributes,
        "#value" => $title,
      ];
    }

    $build['sitemorse'] = [
      '#type' => 'toolbar_item',
      'tab' => $link,
      '#weight' => 99,
    ];

    return $build;
  }

}
