<?php

namespace Drupal\sitemorse_lite\Sitemorse;

/**
 * Results from Sitemorse.
 */
class Results {

  /**
   * The results as returned from the sitemorse library.
   *
   * @var array
   */
  private $results;

  /**
   * Create the Results object from the results of the sitemorse check.
   *
   * @param array $results
   *   The results from sitemorse.
   *
   * @return static
   *   A Results object.
   */
  public static function fromTestResults(array $results) {
    $res = new static();
    $res->setResults($results);

    return $res;
  }

  /**
   * Return the results.
   *
   * @return array
   *   The results from sitemorse.
   */
  public function getResults() {
    return $this->results;
  }

  /**
   * A sitemorse snapshot url.
   *
   * @return string
   *   The URL.
   */
  public function getResultUrl() {
    return $this->results['url'];
  }

  /**
   * Do the results have any errors?
   *
   * @return bool
   *   TRUE if there are errors.
   */
  public function hasErrors() {
    return !empty($this->results['error']);
  }

  /**
   * Set the results.
   *
   * @param array $results
   *   The results from sitemorse.
   */
  public function setResults(array $results) {
    $this->results = $results;
  }

}
