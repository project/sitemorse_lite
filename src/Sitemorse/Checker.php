<?php

namespace Drupal\sitemorse_lite\Sitemorse;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Encapsulates various Sitemorse checking procedures called from Drupal.
 */
class Checker implements CheckerInterface {

  /**
   * The SCI Client PHP library.
   *
   * @var \SCIClient
   */
  protected $client;

  /**
   * ModuleHandler service for invoking hooks.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * Constructor - set up PHP client library.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, \SCIClient $client, AccountInterface $user) {
    $this->moduleHandler = $moduleHandler;
    $this->client = $client;
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public function checkUrl($url): Results {
    $response = $this->client->performTest(
      $url,
      [],
      'snapshot-page',
      '',
      $this->user->id()
    );
    $results = Results::fromTestResults($response);

    $this->moduleHandler->invokeAll('sitemorse_lite_results', [$url, $results]);

    return $results;
  }

}
