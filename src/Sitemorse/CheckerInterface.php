<?php

namespace Drupal\sitemorse_lite\Sitemorse;

/**
 * Defines an interface for a Sitemorse Checker class.
 */
interface CheckerInterface {

  /**
   * Return the sitemorse results for a URL.
   *
   * @param string $url
   *   The absolute URL for sitemorse to check.
   *
   * @return \Drupal\sitemorse_lite\Sitemorse\Results
   *   The results of the sitemorse check.
   */
  public function checkUrl($url): Results;

}
