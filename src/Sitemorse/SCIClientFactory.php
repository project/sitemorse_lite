<?php

namespace Drupal\sitemorse_lite\Sitemorse;

use Drupal\Core\Config\ConfigFactory;

/**
 * Create a SCIClient from config.
 */
class SCIClientFactory {

  /**
   * SCIClientFactory constructor.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->config = $configFactory->get('sitemorse_lite.settings');
  }

  /**
   * Create a configured SCIClient instance.
   *
   * @return \SCIClient
   *   The configured SCIClient instance.
   */
  public function create() {
    return new \SCIClient($this->config->get('license_key'), $this->mapConfig());
  }

  /**
   * Map module config to SCIClient library compatible settings.
   *
   * @return array
   *   The SCIClient settings.
   */
  private function mapConfig() {
    $map = [
      'ssl_connection' => 'serverSecure',
      'post_allowed' => 'postAllowed',
      'proxy_hostname' => 'proxyHostname',
      'proxy_port' => 'proxyPort',
      'server_hostname' => 'serverHostname',
      'server_port' => 'serverPort',
      'additional_http_headers' => 'extraHeaders',
      'additional_query_string' => 'extraQuery',
    ];

    $args = [];

    foreach ($map as $configName => $libraryConfigName) {
      $value = $this->config->get($configName);

      if ($value !== NULL && $value !== '') {
        $args[$libraryConfigName] = $value;
      }
    }

    return $args;
  }

}
