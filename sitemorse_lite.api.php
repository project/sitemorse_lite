<?php

/**
 * @file
 * API for sitemorse_lite module.
 */

use Drupal\sitemorse_lite\Sitemorse\Results;

/**
 * React to sitemorse check results.
 *
 * @param string $url
 *   The URL that has been checked.
 * @param \Drupal\sitemorse_lite\Sitemorse\Results $results
 *   The results.
 */
function hook_sitemorse_lite_results($url, Results $results) {
  $snapshotUrl = $results->getResultUrl();
}
