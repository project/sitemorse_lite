/**
 * @file
 * Provides the JavaScript for Sitemorse Drupal behaviours.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.sitemorse_lite = {
    attach: function (context) {
      const checkLink = 'a.sitemorse-check-url';

      if ($(checkLink).length > 0) {
        processCheckLink($(checkLink));
      }

      function processCheckLink($link) {
        $link.once("processCheckLink").on('click', function (event) {
          event.preventDefault();

          if ($('.sitemorse-popup-wrapper').length < 1) {

            $('body').append('<div class="sitemorse-popup-wrapper" />');
            $('.sitemorse-popup-wrapper').hide();
            $('.sitemorse-popup-wrapper')
              .append('<div class="sitemorse-popup-overlay" style="position: fixed; width: 100%; height: 100%; z-index: 9997; background: rgba(0,0,0,0.5); top: 0;"></div>')
              .append('<div class="sitemorse-popup-close" style="cursor: pointer; position: fixed; width: 50px; height: 50px; right: 50px; top: 50px; z-index: 9999; background: url(/' + drupalSettings.sitemorse_lite.modulePath + '/images/cross.png) no-repeat"></div>')
              .append('<div class="sitemorse-popup-content" style="width: 90%; height: 80%; position: fixed; top: 10%; left: 5%; background: #FFF; z-index: 9998;"><div id="sitemorse-popup-content-url"></div><iframe width="100%" height="100%" border="0" style="border: 0 none;"/></div>');

            $('.sitemorse-popup-close').click(function () {
              $('.sitemorse-popup-wrapper').hide();
            });
          }
          else {
            $('.sitemorse-popup-content-url').html("");
          }

          sitemorseAnimate();

          $.ajax({
            url: drupalSettings.sitemorse_lite.callback,
            method: "GET",
            timeout: 0,
            data: {'url': window.location.href},
            success: function (data) {
              $('#darkcover').remove();
              if (data.url !== undefined) {
                $('.sitemorse-popup-content iframe').attr('src', data.url);
                $('#sitemorse-popup-content-url').html('<a href="' + data.url + '" target="_blank">View full screen</a>');
                $('.sitemorse-popup-wrapper').show();
              }
            },
            error: function (data) {
              $('#darkcover').remove();
              let error = '';
              if (data.responseJSON) {
                error = data.responseJSON.error;
              }
              let output = Drupal.t(
                "<p>An error occurred retrieving the Sitemorse results. The error was: <strong>@error</strong></p><p>Contact your Drupal CMS administrator about this message.</p>",
                {'@error': error}
              );
              let adminOutput = '';
              if (drupalSettings.sitemorse_lite.moduleConfigAccess) {
                adminOutput = Drupal.t(
                  "<p>You may need to check your <a href=\"@module-configuration-page\">module configuration</a>.</p>",
                  {'@module-configuration-page': drupalSettings.sitemorse_lite.moduleConfigPath.toString()}
                );
              }
              $('.sitemorse-popup-content').html('<div class="sitemorse-error-container"><div class="messages messages--error">' + output + adminOutput + '</div></div>');
              $('.sitemorse-popup-wrapper').show();
            }
          });

          return false;
        });
      }

      function sitemorseAnimate() {
        $('#darkcover').remove();
        $('body').append('<div id="darkcover" />');
        $('#darkcover').click(function () {
          $(this).toggle();
        }).append('<div id="sciLoading" />');
        $('#sciLoading').click(function (e) {
          e.stopPropagation();
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
