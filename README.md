Sitemorse Lite
===

Drupal 8 integration with the [Sitemorse][1] service.

## Introduction
The Sitemorse module provides an integration to the Sitemorse service.


## Requirements
This module requires the following:

* Sitemorse license key - contact your Sitemorse representative to obtain a
  license key.

## Installation

* `composer require drupal/sitemorse_lite`

## Configuration
Configure user permissions in Administration » People » Permissions:

* Administer Sitemorse settings
  Only users with the "Administer Sitemorse settings" permission are
  allowed to access the module configuration page.
* Run sitemorse check
  Only users with this permission are permitted to run the sitemorse
  check.

Configure the module settings in Administration » Configuration » Content
Authoring » Sitemorse settings:

* Enter a licence key
* Verify the licence key

## Credit
* Original code by Duncan Davidson (ded) - https://drupal.org/u/ded
* Drupal 8 port by Ixis - https://ixis.co.uk/

[1]: <https://sitemorse.com/> "Sitemorse"
